using Avalonia;
using Avalonia.Markup.Xaml;

namespace Updater {

    public class App : Application {

        public override void Initialize() {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
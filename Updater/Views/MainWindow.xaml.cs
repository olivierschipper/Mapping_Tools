using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Updater.Models;

namespace Updater.Views {

    public class MainWindow : Window {

        public MainWindow() {
            var Model = new MainWindowModel();
            InitializeComponent();
        }

        private void InitializeComponent() {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
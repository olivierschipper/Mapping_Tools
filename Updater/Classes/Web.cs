﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Updater.Classes.Web {

    internal abstract class Web {

        public static async Task<JObject> GetJSON(string path) {
            if (Uri.TryCreate(path, UriKind.RelativeOrAbsolute, out var uri)) {
                using var client = new HttpClient();
                var responseString = "";

                try {
                    using var response = await client.GetAsync(uri);
                    responseString = await response.Content.ReadAsStringAsync();

                    return JObject.Parse(responseString);
                }
                catch (Exception) {
                    throw new GetJSONException();
                }
            }
            else {
                throw new GetJSONException();
            }
        }
    }

    [Serializable]
    internal class GetJSONException : Exception {
    }
}
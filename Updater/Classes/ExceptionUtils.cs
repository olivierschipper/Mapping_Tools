﻿using System;

namespace Updater.Classes.Exceptions {

    public class GetJSONException : Exception { }

    public class PathParseException : Exception { }

    public class UriParseException : Exception { }

    public class VersionParseException : Exception { }

    public class DownloadException : Exception { }

    public class MD5HashException : Exception { }

    public class CleanupException : Exception { }
}
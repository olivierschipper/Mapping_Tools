﻿using System;
using System.Windows;

namespace Mapping_Tools.Views.SnappingTools {

    /// <summary>
    /// Interaction logic for SnappingOverlay.xaml
    /// </summary>
    public partial class SnappingOverlay : Window {

        public SnappingOverlay() {
            InitializeComponent();
        }

        private void Window_Deactivated(object sender, EventArgs e) {
            Window window = (Window)sender;
            window.Topmost = true;
        }
    }
}
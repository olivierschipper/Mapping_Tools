﻿namespace Mapping_Tools.Classes.BeatmapHelper {

    internal interface ITextLine {

        string GetLine();

        void SetLine(string line);
    }
}
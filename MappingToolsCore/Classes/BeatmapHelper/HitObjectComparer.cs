﻿using System.Collections.Generic;

namespace Mapping_Tools.Classes.BeatmapHelper {

    internal class HitObjectComparer : IEqualityComparer<HitObject> {

        public bool Equals(HitObject x, HitObject y) {
            return x.GetLine() == y.GetLine();
        }

        public int GetHashCode(HitObject obj) {
            return EqualityComparer<string>.Default.GetHashCode(obj.GetLine());
        }
    }
}
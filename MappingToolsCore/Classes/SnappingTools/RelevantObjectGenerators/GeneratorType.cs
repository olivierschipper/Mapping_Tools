﻿namespace Mapping_Tools.Classes.SnappingTools.RelevantObjectGenerators {

    public enum GeneratorType {
        Basic,
        Polygons,
        Blankets,
        Geometries,
        Symmetries
    }
}
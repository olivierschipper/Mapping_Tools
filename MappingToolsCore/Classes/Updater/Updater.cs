﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MappingToolsCore.Classes.Updater {

    public enum UpdaterStatus {
        CheckingUpdates,
        ReadyToUpdate,
        Downloading,
        Finished,
        NoUpdates,
        CommonError,
        NoInternet,
        DownloadError,
        NoAccess,
        NoAccessDelete
    }

    public class Updater {

        public delegate void StatusChanged();

        public event StatusChanged StatusUpdate;

        public UpdaterStatus Status { get; set; } = UpdaterStatus.CheckingUpdates;
        public string UpdateChangelog { get; set; }

        private readonly List<ManifestItem> Manifest = new List<ManifestItem>();
        private static readonly string Domain = "https://mappingtools.seira.moe";
        private static readonly string ExecutionPath = AppDomain.CurrentDomain.BaseDirectory;
        private static readonly string ServerPathToRemove = "/srv/mappingtools/backend/data";
        private static readonly string LocalPathToRemove = "/updater/resources";

        public async void Prepare() {
            var gotManifest = await GetManifest();
            var informationJSON = await GetInformationJSON();

            if (informationJSON != null) {
                UpdateChangelog = (string)informationJSON["version"];
                if (Version.TryParse((string)informationJSON["version"], out var version)) {
                    var currentAssembly = Assembly.GetEntryAssembly().GetName().Version;
                    if (currentAssembly.CompareTo(version) < 0) {
                        if (gotManifest.GetValueOrDefault()) {
                            Status = UpdaterStatus.ReadyToUpdate;
                            StatusUpdate();
                        }
                        else {
                            if (Status == UpdaterStatus.CheckingUpdates) {
                                Status = UpdaterStatus.CommonError;
                                StatusUpdate();
                            }
                        }
                    }
                    else {
                        if (Status == UpdaterStatus.CheckingUpdates) {
                            Status = UpdaterStatus.NoUpdates;
                            StatusUpdate();
                        }
                    }
                }
                else {
                    if (Status == UpdaterStatus.CheckingUpdates) {
                        Status = UpdaterStatus.CommonError;
                        StatusUpdate();
                    }
                }
            }
            else {
                if (Status == UpdaterStatus.CheckingUpdates) {
                    Status = UpdaterStatus.CommonError;
                    StatusUpdate();
                }
            }
        }

        public void Update() {
            if (Status == UpdaterStatus.ReadyToUpdate) {
                AdjustDirectories();
                AnalyzeManifest();
                CleanupFiles(ExecutionPath);
                Status = UpdaterStatus.Finished;
                StatusUpdate();
            }
        }

        private async Task<JObject> GetInformationJSON() {
            if (Uri.TryCreate("https://mappingtools.seira.moe/current/updater.json", UriKind.RelativeOrAbsolute, out var uri)) {
                using var client = new HttpClient();
                var responseString = "";

                try {
                    using var response = await client.GetAsync(uri);
                    responseString = await response.Content.ReadAsStringAsync();

                    return JObject.Parse(responseString);
                }
                catch (HttpRequestException httpex) {
                    Console.WriteLine(httpex.StackTrace);
                    Status = UpdaterStatus.NoInternet;
                    StatusUpdate();
                    return null;
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.StackTrace);
                    Status = UpdaterStatus.CommonError;
                    StatusUpdate();
                    return null;
                }
            }

            Status = UpdaterStatus.CommonError;
            StatusUpdate();
            return null;
        }

        private async Task<bool?> GetManifest() {
            if (Uri.TryCreate("https://mappingtools.seira.moe/updater/manifest.json", UriKind.RelativeOrAbsolute, out var uri)) {
                using var client = new HttpClient();
                var responseString = "";

                try {
                    using var response = await client.GetAsync(uri);
                    responseString = await response.Content.ReadAsStringAsync();

                    JObject linqJson = JObject.Parse(responseString);
                    var linqArr = (JArray)linqJson["paths"];

                    Manifest.Clear();

                    foreach (var item in linqArr) {
                        var path = ((string)item["path"]).Replace(ServerPathToRemove, "");
                        var serverPath = Domain + path;
                        var localPath = path.Replace(LocalPathToRemove, "");
                        localPath = localPath.Remove(0, 1);
                        localPath = Path.Combine(ExecutionPath, localPath);

                        if (Uri.TryCreate(serverPath, UriKind.Absolute, out var serverPathUri)) {
                            if (Uri.TryCreate(localPath, UriKind.Absolute, out var localPathUri)) {
                                var name = localPath.Split(Path.DirectorySeparatorChar).Last();
                                var manifestItem = new ManifestItem {
                                    ServerPath = serverPathUri,
                                    LocalPath = localPathUri,
                                    Hash = (string)item["hash"],
                                    Name = name,
                                    IsFile = (bool)item["isFile"]
                                };

                                Manifest.Add(manifestItem);
                            }
                            else {
                                throw new Exception();
                            }
                        }
                        else {
                            throw new Exception();
                        }
                    }

                    return true;
                }
                catch (HttpRequestException httpex) {
                    Console.WriteLine(httpex.StackTrace);
                    Status = UpdaterStatus.NoInternet;
                    StatusUpdate();
                    return false;
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.StackTrace);
                    Status = UpdaterStatus.CommonError;
                    StatusUpdate();
                    return false;
                }
            }

            Status = UpdaterStatus.CommonError;
            StatusUpdate();
            return false;
        }

        private void AdjustDirectories() {
            try {
                foreach (var item in Manifest) {
                    if (!item.IsFile) {
                        Directory.CreateDirectory(item.LocalPath.LocalPath);
                    }
                }
            }
            catch (UnauthorizedAccessException uaex) {
                Console.WriteLine(uaex.StackTrace);
                Status = UpdaterStatus.NoAccess;
                StatusUpdate();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.StackTrace);
                Status = UpdaterStatus.CommonError;
                StatusUpdate();
            }
        }

        private void AnalyzeManifest() {
            try {
                foreach (var item in Manifest) {
                    if (item.IsFile) {
                        if (File.Exists(item.LocalPath.LocalPath)) {
                            using var md5 = MD5.Create();
                            using var fileStream = File.OpenRead(item.LocalPath.LocalPath);
                            var hash = md5.ComputeHash(fileStream);
                            var hashString = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                            fileStream.Dispose();
                            if (!hashString.Equals(item.Hash)) {
                                File.Delete(item.LocalPath.LocalPath);
                                DownloadFile(item);
                            }
                        }
                        else {
                            DownloadFile(item);
                        }
                    }
                    else {
                        //
                    }
                }
            }
            catch (UnauthorizedAccessException ex) {
                Console.WriteLine(ex.StackTrace);
                Status = UpdaterStatus.NoAccess;
                StatusUpdate();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.StackTrace);
                Status = UpdaterStatus.CommonError;
                StatusUpdate();
            }
        }

        private async void DownloadFile(ManifestItem item) {
            try {
                using var client = new WebClient();
                await client.DownloadFileTaskAsync(item.ServerPath, item.LocalPath.LocalPath);
            }
            catch (WebException wex) {
                Console.WriteLine(wex.StackTrace);
                Status = UpdaterStatus.NoInternet;
                StatusUpdate();
            }
            catch (InvalidOperationException ioex) {
                Console.WriteLine(ioex.StackTrace);
                Status = UpdaterStatus.NoAccess;
                StatusUpdate();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.StackTrace);
                Status = UpdaterStatus.CommonError;
                StatusUpdate();
            }
        }

        private void CleanupFiles(string dirPath) {
            try {
                var files = Directory.GetFiles(dirPath);
                foreach (var file in files) {
                    var result = Manifest.SingleOrDefault(s => s.LocalPath.LocalPath == file);
                    if (result == null) {
                        try {
                            File.Delete(file);
                        }
                        catch (Exception) {
                            // IGNORE DELETING AS FILES USED BY THE SYSTEM CAN NOT BE DELETED DURING RUNTIME
                        }
                    }
                }

                var dirs = Directory.GetDirectories(dirPath);
                if (dirs.Length > 0) {
                    foreach (var dir in dirs) {
                        CleanupFiles(dir);
                    }
                }
                else {
                    var result = Manifest.SingleOrDefault(s => s.LocalPath.LocalPath == dirPath);
                    if (result == null) {
                        if (Directory.GetFiles(dirPath).Length == 0 && Directory.GetDirectories(dirPath).Length == 0) {
                            try {
                                Directory.Delete(dirPath);
                            }
                            catch (Exception) {
                                // IGNORE DELETING AS DIRS USED BY THE SYSTEM CAN NOT BE DELETED DURING RUNTIME
                            }
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException uaex) {
                Console.WriteLine(uaex.StackTrace);
                Status = UpdaterStatus.NoAccessDelete;
                StatusUpdate();
            }
        }
    }

    internal class ManifestItem {
        public Uri ServerPath { get; set; }
        public Uri LocalPath { get; set; }
        public string Hash { get; set; }
        public string Name { get; set; }
        public bool IsFile { get; set; }
    }
}